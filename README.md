# MikroTik Ansible Test

MikroTik Ansible modules have recently been added to the devel branch of Ansible. You can view the merge request over here: https://github.com/ansible/ansible/pull/41155.

## Setup

Install Ansible from source. (https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#running-from-source)
clone this playbook: `git clone https://gitlab.com/33Fraise33/mikrotik-ansible-test.git`

Bootup a virtual MikroTik device with a CHR image: http://www.mikrotik.com/download#chr or connect a router with default configuration to your pc. Change the 'development' inventory file to the correct ip address.

Run the playbook: `ansible-playbook -i development testplay.yaml` (it will ask for a new router name during the play).
